<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;



    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    // protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function show(){
        if(Auth::guard('admin')->check()){
            return redirect('/dashboard');
        }
        return view('auth.login');
    }

    public function login(Request $request){
        if($request->level='admin'){
            if (auth()->guard('admin')->attempt(
              ['email'    => $request->email, 
              'password' => $request->password,
              
            ]))
              
            {
              return redirect()->route('dashboard');
            } 
          }
          return back()->withErrors(['email' => 'Email or password are wrong.']);

    }

    public function logout(){
        if (Auth::guard('admin')->check()) {
            Auth::guard('admin')->logout();
          } 
        return redirect('backend');
    }
}
