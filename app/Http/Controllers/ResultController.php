<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Model\Number;
use Validator;
use Carbon\Carbon;
use App\Model\Admin;
use Auth;
use Illuminate\Support\Facades\DB; 

class ResultController extends Controller
{
    public function result(){
    	$data = DB::table('number')->orderBy('tanggal','desc')->get();
        return view('result',compact('data'));
    }
}
