@extends('layouts2.master2')
@section('css')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
@endsection
@section('content')

	<div class="container">

	<table border="0" class="resultsTable table" cellspacing="1" cellpadding="5" width="100%">
		<thead align="center">
			<tr style="background-color: #2768ab;">
				<th style="font-size: 20px; width: 100%">More Result</th>
				<td></td>
			</tr>
		</thead>
	</table>

	<br>
	<table id="datatable" class="table table-striped table-bordered" style="width:100%">
		<thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>1st Place</th>
                <th>2nd Place</th>
                <th>3rd Place</th>
            </tr>
        </thead>
        <tbody>
        	<?php foreach ($results as $item): ?>
        		@if($item->tanggal->format('Y-m-d') != $skr && $item->tanggal->format('Y-m-d') != $tom)
        		<tr>
        			<td>{{ $item->tanggal->format('d-m-Y') }}</td>
        			<td>{{ $item->time }}</td>
        			<td>{{ $item->angka_1 }}</td>
        			<td>{{ $item->angka_2 }}</td>
        			<td>{{ $item->angka_3 }}</td>




        		</tr>
        		@endif
        	<?php endforeach ?>
        </tbody>


	</table>
</div>


</div>
<br>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
	$(document).ready(function() {
    $('#datatable').DataTable({
    	ordering:false,
    	responsive: true

    })
    ;
} );
</script>

@endsection