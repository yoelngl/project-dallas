@extends('layouts2.master2')
@section('content')

<script>
	function AppSlide(className){
		const slideOne = document.querySelector(".slideOne");
		const slideTwo = document.querySelector(".slideTwo");
		const slideThree = document.querySelector(".slideThree");
		const button = document.querySelector(".appBox .button");

		setTimeout(function(){
			slideOne.classList.add(className);
		},400);
		setTimeout(function(){
			slideTwo.classList.add(className);
		},800);
		setTimeout(function(){
			slideThree.classList.add(className);
		},1200);
		setTimeout(function(){
			button.classList.add(className);
		},1600);
	}
	AppSlide("slideActive");
</script>

		<script>
		<!--
			function hover(obj,state) {
				if (state == 'on' && obj.className.indexOf('expanded') == -1) {
				    obj.parentNode.className += ' hovered'
				} else {
				    obj.parentNode.className = obj.parentNode.className.replace(' hovered', '');
				}
			}

	
		</script>
			<div class="container-fluid">
				<div class="row">
					<div class="col-sm-12" style="padding-left: 0px;margin-top: -20px;">
						<img src="{{asset ('assets/images/dallascity3.jpg')}}" style="height: 400px;width: 1350px;">
					</div>
				</div>
			</div>
			<br>
			<br>

<div class="container">
		
{{-- Data Winner --}}
<div class="flexRow">
	<div class="twoCol">
		<table class="resultsTable table topTwo">
				<tr style="background: #ff0000; border: none; padding: 0;">
					<th class="lefty" style="background: #ff0000; border: none; padding: 10px;">
						<div style="color: #FFF; padding: 0; margin: 0; font-size: 1.4em; font-weight: 400; display: inline-block; letter-spacing: 0;"><?= $days ?>'s Result</div>
					</th>
				</tr>
				<tr class="dateRow" style="border: none;">
					<td style="padding: 5px 10px; background: #a30a0a; color: #FFF; border: none; text-align: left;">
						<?= $tanggal; ?> / {{ $hari }}
					</td>
				</tr>
					
				<tr style="border: none;">
					<td style="text-align: center; border: none;">
						
						@foreach ($data as $item)
						<br>
						
					<div class="container">
						<div class="row">
							<div class="col-sm-4">
								<div class="wpb_wrapper">
									<div class="wpb_single_image wpb_content_element vc_align_center">
						<figure class="wpb_wrapper vc_figure">
							<div class="vc_single_image-wrapper   vc_box_border_grey">
								<img width="121" height="200" src="https://posi.or.id/wp-content/uploads/2020/04/juara1.jpg" class="vc_single_image-img attachment-full" alt=""></div>
						</figure>
					</div>
				<div class="prelude-icon-box clearfix icon-top align-center  link-hover-accent simple" style=""><div class="wrap-inner" style="position: relative;"> 
					<div class="content-wrap"><h3 class="heading " style="font-size:22px;margin-top:25px;margin-bottom:14px;">
							<ul class="balls small" style="margin-right: -12px;">
							<?php for($i = 0; $i <= 3; $i++) : ?>
								<img src="{{ asset('assets/images/dallas/'.$item->angka_1[$i].'.png') }}" width="30px" height="40px" class="mr-2" alt="" /> 
						<?php endfor; ?>
							 {{-- <img src="{{ asset('assets/images/dallas/2.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 

								<img src="{{ asset('assets/images/dallas/3.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 
							
								<img src="{{ asset('assets/images/dallas/4.png') }}" width="30px" height="40px" class="mr-3" alt="" />  --}}
						</ul>

						</h3>
					</div>
				</div>
			</div>
			<div class="prelude-divider divider-center divider-solid" style="width:30px;border-width:5px;border-top-color:#e7e7e7;margin-top:23px;"></div><div class="clearfix">
				
			</div>
					<div class="prelude-spacer clearfix" data-desktop="30" data-mobi="30" data-smobi="30" style="height:30px"></div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="wpb_wrapper">
					<div class="wpb_single_image wpb_content_element vc_align_center">
						<figure class="wpb_wrapper vc_figure">
							<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="121" height="200" src="https://posi.or.id/wp-content/uploads/2020/04/juara2.jpg" class="vc_single_image-img attachment-full" alt=""></div>
						</figure>
					</div>
				<div class="prelude-icon-box clearfix icon-top align-center  link-hover-accent simple" style="">
					<div class="wrap-inner" style="position: relative;"> 
						<div class="content-wrap">
							<h3 class="heading " style="font-size:22px;margin-top:25px;margin-bottom:14px;">
							<ul class="balls small" style="margin-left: 10px;">
							<?php for($i = 0; $i <= 3; $i++) : ?>
								<img src="{{ asset('assets/images/dallas/'.$item->angka_2[$i].'.png') }}" width="30px" height="40px" class="mr-2" alt="" /> 
						<?php endfor; ?>
							
								{{-- <img src="{{ asset('assets/images/dallas/2.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 

								<img src="{{ asset('assets/images/dallas/3.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 
							
								<img src="{{ asset('assets/images/dallas/4.png') }}" width="30px" height="40px" class="mr-3" alt="" />  --}}
							</ul>
							</h3>
						</div>
					</div>
				</div>
				<div class="prelude-divider divider-center divider-solid" style="width:30px;border-width:5px;border-top-color:#e7e7e7;margin-top:23px;"></div>
				<div class="clearfix"></div>
					<div class="prelude-spacer clearfix" data-desktop="30" data-mobi="30" data-smobi="30" style="height:30px"></div>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="wpb_wrapper">
					<div class="wpb_single_image wpb_content_element vc_align_center">
						<figure class="wpb_wrapper vc_figure">
							<div class="vc_single_image-wrapper   vc_box_border_grey"><img width="121" height="200" src="https://posi.or.id/wp-content/uploads/2020/04/juara3.jpg" class="vc_single_image-img attachment-full" alt=""></div>
						</figure>
					</div>
				<div class="prelude-icon-box clearfix icon-top align-center  link-hover-accent simple" style=""><div class="wrap-inner" style="position: relative;"> <div class="content-wrap"><h3 class="heading " style="font-size:22px;margin-top:25px;margin-bottom:14px;">
							<ul class="balls small" style="margin-right:-10px;">
							<?php for($i = 0; $i <= 3; $i++) : ?>
							<img src="{{ asset('assets/images/dallas/'.$item->angka_3[$i].'.png') }}" width="30px" height="40px" class="mr-2" alt="" /> 
					<?php endfor; ?>
								{{-- <img src="{{ asset('assets/images/dallas/2.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 

								<img src="{{ asset('assets/images/dallas/3.png') }}" width="30px" height="40px" class="mr-3" alt="" /> 
							
								<img src="{{ asset('assets/images/dallas/4.png') }}" width="30px" height="40px" class="mr-3" alt="" />  --}}
							</ul>
							@endforeach
						</h3>
					</div>
				</div>
			</div>
			</div>
		</div>
	</div> 
</div>

		</tr>		
			</table>

		</div>
</div>

		<br>

		<div class="flexRow">
			<div class="twoCol" style="background: no-repeat;">

				<h2 style="font-size: 40px;text-align: center;color: red;font-family: 'Titillium Web', sans-serif;"><strong> Past Winning Number </strong></h2>
			</div>
		</div>
		
		<div class="row">
            <div class="col-sm-12">
                <table class="table" style="font-size: 18px;font-family: 'Titillium Web', sans-serif;">
                    <thead class="text-center">
                        <tr style="background-color: red; height: 48px;">
                            <th style="font-size: 18px;"><strong> Date <strong></th>
                            <th style="font-size: 18px;"><strong> Morning <strong></th>
                            <th style="font-size: 18px;"><strong> Midday <strong></th>
                            <th style="font-size: 18px;"><strong> Evening <strong></th>
                            <th style="font-size: 18px;"><strong> Night <strong></th>
                        </tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $tadi }}</td>
						    <td style="background-color: #ffffff;">
						    	@if($pagiTadi == null)
								-
						    	@else
						    	@for($i=0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pagiTadi[$i].'.png')}}" alt="" width="auto" height="30px">
								@endfor
								@endif
						    </td>
						    <td style="background-color: #ffffff;">
						       @if($siangTadi == null)
								-
						    	@else
						    	@for($i=0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $siangTadi[$i].'.png')}}" alt="" width="auto" height="30px">
								@endfor
								@endif
						    </td>
						    <td style="background-color: #ffffff;">
						     @if($soreTadi == null)
								-
						    	@else
						    	@for($i=0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $soreTadi[$i].'.png')}}" alt="" width="auto" height="30px">
								@endfor
								@endif
						    </td>
						    <td style="background-color: #ffffff;">
						       @if($malamTadi == null)
								-
						    	@else
						    	@for($i=0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $malamTadi[$i].'.png')}}" alt="" width="auto" height="30px">
								@endfor
								@endif
						    </td>
						</tr>





						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd2 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd2)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd2)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd2)
						    <td style="background-color: #cccccc;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd2)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd3 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd3)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd3)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd3)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd3)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd4 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd4)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd4)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd4)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd4)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd5 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd5)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd5)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd5)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd5)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd6 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd6)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd6)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd6)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd6)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>

						<tr>
							<td class="tanggal" style="background-color: #ffffff;">{{ $ytd7 }}</td>
							@foreach($pagi as $pg)
							@if($pg->tanggal->format('M j') == $ytd7)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $pg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						    @foreach($siang as $sg)
							@if($sg->tanggal->format('M j') == $ytd7)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sg->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach
						   
							@foreach($sore as $sr)
							@if($sr->tanggal->format('M j') == $ytd7)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $sr->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    
						    @endif
						    @endforeach

						   

						    @foreach($malam as $mm)
							@if($mm->tanggal->format('M j') == $ytd7)
						    <td style="background-color: #ffffff;">
						        @for($i = 0; $i <= 3; $i++)
						        <img class="mobile" src="{{asset ('assets/images/dallas/'. $mm->angka_1[$i] .'.png')}}" alt="" width="auto" height="30px">
						        @endfor
						       
						    </td>
						    @endif
						    @endforeach
						</tr>
                    </thead>
                    
                </table>

                <div class="row">
                	<div class="col-sm-5"></div>
                	<div class="col-sm-2">
                		<a href="{{ route('result') }}" title="View draw details and prize breakdown for Tuesday 2nd June 2020" class="nav-link link btn btn-large btn-rounded btn-white text-dark" target="_blank"><strong><font color="white"> More Results </font></strong></a>
                	</div>
                </div>

                <br>
                <br>
                <br>
                <br>

                <img src="http://versailleslottery.com/images/bnv.png" alt="" class="float-left" height="45">
                <img src="http://versailleslottery.com/images/vip.png" alt="" class="float-right">

                <br>
                <br>

            </div>
        </div>

	
	</div>
	

@stop
