<!DOCTYPE html>



<html lang="en">


<head>

	<title>EuroMillions - News and Information</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="description" content="EuroMillions is Europe's biggest lottery and offers jackpots of up to €200 million. EuroMillions draws take place on Tuesday and Friday nights.">
	<meta name="keywords" content="euromillions, play euromillions, euromillions lottery, euromillions results">
	<meta name="author" content="Euro-Millions.com">
	<meta name="format-detection" content="telephone=no">
	<meta name="HandheldFriendly" content="True">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="apple-touch-fullscreen" content="yes">
	
	<link rel="alternate" hreflang="x-default" href="index.html">
	<link rel="alternate" hreflang="fr" href="fr/index.html">
	<link rel="alternate" hreflang="de" href="de/index.html">
	<link rel="alternate" hreflang="de-at" href="de-at/index.html">
	<link rel="alternate" hreflang="de-ch" href="de-ch/index.html">
	<link rel="alternate" hreflang="pt" href="pt/index.html">
	<link rel="alternate" hreflang="es" href="es/index.html">
	<link rel="alternate" hreflang="it" href="it/index.html">
	<link rel="alternate" hreflang="sv" href="sv/index.html">
	<link rel="alternate" hreflang="nl" href="nl/index.html">
	<link rel="alternate" hreflang="ru" href="ru/index.html">
	<link rel="alternate" hreflang="pl" href="pl/index.html">
	<link rel="alternate" hreflang="zh" href="zh/index.html">
	<link rel="alternate" hreflang="th" href="th/index.html">
	<link rel="alternate" hreflang="ar" href="ar/index.html">
	<link rel="alternate" hreflang="ro" href="ro/index.html">
	<link rel="stylesheet" href="{{ asset('includes/styleea16.css') }}">

	<meta property="og:title" content="EuroMillions - News and Information">
	<meta property="og:description" content="EuroMillions is Europe's biggest lottery and offers jackpots of up to €200 million. EuroMillions draws take place on Tuesday and Friday nights.">
	<meta property="og:type" content="website">
	<meta property="og:url" content="index.html">
	<meta property="og:site_name" content="EuroMillions">
	<meta property="og:image" content="images/facebook-shared-image.jpg" />
	<meta property="fb:app_id" content="124973790926555">
	<meta property="fb:admins" content="100001278704026">
	
	<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Rubik:400,500,700&amp;display=swap">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="includes/styleea16.css?v=20200514155308">

	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js" integrity="sha384-OgVRvuATP1z7JjHLkuOU7Xw704+h835Lr+6QL9UvYjZE3Ipu6Tp75j7Bh/kR0JKI" crossorigin="anonymous"></script>
		
	
	
	<style>
		body {background: #EEE;}
a.button {border-radius: 0; font-weight: 400; border: none !important;}
a.button:hover {border: none !important;}
.resultsTable, .resultsTable tbody td {border: none; background: #FFF; margin: 0;}
.modBox {background: #FFF; padding: 20px; box-sizing: border-box; overflow: hidden;}
.modBox.dark {background: url(images/stars-bg.png) #de1010 center no-repeat; background-size: cover; color: #FFF;}
.modBox.full {width: 100%; margin-bottom: 20px;}
.modBox h1, .modBox h2, .modBox h3, .modBox .h2, .modBox .h3 {background: none; margin: 0 0 10px 0; padding: 0; color: #ff0000; width: 100%; float: none;}
.modBox.dark h2, .modBox.dark h3 {color: #FFF;}
.modBox p {margin: 0; padding: 10px 0 0; font-size: 15px; line-height: 24px;}
.modBox a.button {
	margin-top: 20px; box-shadow: none; border: none !important; color: #FFF !important; padding: 8px 15px; background: #ff0000; display: block; text-align: center;
	background: linear-gradient(to bottom,  #ff0000 0%,#ff0000 50%,#a30a0a 50%,#a30a0a 100%);
}
.modBox a.button:hover {background: #4d9eea; background: linear-gradient(to bottom, #4d9eea 0%,#458ed2 50%,#2e79c6 51%,#286aaf 100%);}

ol.how-to-play {margin: 0;}
ol.how-to-play li {padding: 10px; line-height: 24px; list-style-type: none; margin: 0;}
ol.how-to-play li:before {content: attr(data-pos); position: relative; background: #FFF; color: #333; width: 40px; height: 40px; text-align: center; font: 400 20px/40px rubik; border-radius: 30px; margin-right: 10px; display: inline-block;}



	.container {width: 1200px; overflow: hidden;}
	#content.wide {background: none; border: none; width: 930px; padding: 0;}
	
	#menu-container {width: 250px;}
	#menu-container #menu, #menu-container .european, #menu-container .appBox {width: auto; box-sizing: border-box;}
	#menu-container #menu, #menu-container .european {background: #ff0000; border: none; padding-bottom: 10px;}
	#menu-container .top {background: #a30a0a; margin: 0; padding: 5px 10px; width: auto !important;}
	#menu > ul a, .european > ul a {padding: 2px 10px; transition: none !important;}
	#menu > ul > li, .european > ul > li {padding: 0 10px !important;}
	#menu > ul li:hover, .european > ul li:hover {background: #a30a0a !important;}
	#menu-container ul.subMenu {left: 100%;}
	#menu form#cse-search-box-left {width: 100%; box-sizing: border-box; padding: 0 10px;}
	#menu form#cse-search-box-left input[type=text] {width: 190px; border-radius: 0;}
	#menu form#cse-search-box-left input[type=submit] {right: 20px; border-radius: 0;}
	
	#menu-container .casino {width: auto; padding: 0;}
	#menu-container .casino .desktop, #menu-container .casino .desktop img {width: 100%; display: block;}
	#menu-container .casino .button {margin: 0; border-radius: 0; position: absolute; width: 80%; bottom: 20px; left: 10%; box-sizing: border-box;}

	.flexRow {display: flex; flex-direction: row; align-items: stretch; justify-content: space-between; width: 100%; margin-bottom: 20px;}
	.flexRow .twoCol {background: #FFF; width: 49%;}
	.flexTwoThirds {flex: 66% 0 0;}
	
	.modBox.third {width: 32%; display: flex; flex-direction: column;}
	.modBox.third p {flex-grow: 2;}
	
	ul.balls.small .ball.new {width: 36px; height: 36px; line-height: 36px; font-size: 20px; background-color: #f70f0f; border-color: #f70f0f;}
	ul.balls.small .lucky-star.new {width: 40px; height: 40px; line-height: 40px; font-size: 20px;}
	
	.appBox {height: 440px; width: 100% !important;}
	

	</style>
	

	
	<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
	
	<script>function addLoadEvent(e){var t=window.onload;if(typeof window.onload!="function"){window.onload=e}else{window.onload=function(){if(t){t()}e()}}};</script>
	
	<link rel="amphtml" href="amp/index.html">
<script type="application/ld+json">{"@context":"http://schema.org","@type":"WebSite","url":"https://www.euro-millions.com/","potentialAction":{"@type":"SearchAction","target":"https://www.euro-millions.com/search-results?q={search_term_string}","query-input":"required name=search_term_string"}}</script><link rel="canonical" href="index.html">

				<script>
					(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
					(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
					m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
					})(window,document,'script','../www.google-analytics.com/analytics.js','ga');
					
					ga('create','UA-3838501-26','euro-millions.com');
					ga('require','linkid','linkid.html');
					ga('require','displayfeatures');
					ga('send','pageview');
				</script>
			
	
</head>

<body>

	

	<div id="header-container" style="background-color: red;">
		
		<header>
		
			

				<nav>
					<div itemscope itemtype="http://schema.org/Organization" id="logo"><a itemprop="url" href="/home" title="EuroMillions"><img itemprop="logo" src="images/logo.svg" alt="Euro-Millions.com Logo" style="height: 60px;"></a></div>
				</nav>
				
			
				
		</header>
			
	</div>

	@yield('content')

<footer>
	
	<div id="footer">
			
		<div class="top">
			<div class="logoBox">
				<div class="logo"><img src="images/logo.svg" alt="EuroMillions"></div>
				<p class="copyright-info">Material Copyright &copy; 2020</p>
			</div>
			
			<div class="social">
				<span>Follow us to stay up to date</span>
				<ul>
					<li class="twitter"><a href="https://twitter.com/uk_euromillions" title="EuroMillions Twitter" target="_blank" rel="noopener" id="footerTwitter"></a></li>
					<li class="instagram"><a href="https://www.instagram.com/euromillions/" title="EuroMillions Instagram" target="_blank" rel="noopener" id="footerInstagram"></a></li>
					<li class="facebook"><a href="https://www.facebook.com/GB.EuroMillions/" title="EuroMillions Facebook" target="_blank" rel="noopener" id="footerFacebook" onclick="alert('Please note, you will need to be logged in to Facebook to visit our page.')"></a></li>
				</ul>
			</div>
					
		</div>	
		
		<div class="lower">
			<p class="centre" style="font-size: 12px;">
				<a href="about.html" title="About Us - Euro-Millions.com">About Us</a> &bull;
				<a href="privacy-policy.html" title="Privacy Policy">Privacy Policy</a> &bull; 
				<a href="cookie-policy.html" title="Cookie Policy">Cookie Policy</a> &bull; 
				<a href="sitemap.html" title="Sitemap">Sitemap</a> &bull; 
				<a href="contact.html" title="Contact Us">Contact</a> &bull;
				<a href="terms-and-conditions.html" title="Terms and Conditions">Terms and Conditions</a>
			</p>
			<div class="centre logos-sm">
				<a href="https://www.begambleaware.org/" title="Gambleaware" target="_blank" rel="noopener"><img src="images/logos/gambleaware-logo.svg" alt="Gambleaware Logo"></a>
				<p>Players must be 18 or over to participate in online lotteries.</p>
			</div>
			<p class="centre"><a href="disclaimer.html" title="disclaimer">Disclaimer</a>: The content and operations of this website have not been approved or endorsed by Camelot UK Lotteries Limited, the National Lottery Commission or SLE.</p>
		</div>
			
		
	
	</div>

</footer>

<script>
	var toggleLang = document.querySelector('.dropdown-box');
	toggleLang.addEventListener('click', function(){});
</script>

</body>

</html>