@extends('layouts2.master2')
@section('css')
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"> -->
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="{{ asset('includes/styleea16.css') }}">
@endsection
@section('content')

    <div class="container">

    <div class="flexRow">
            <div class="twoCol" style="background: no-repeat;">

                <h2 style="font-size: 40px;text-align: center;color: red;font-family: 'Titillium Web', sans-serif; margin-top:50px; margin-bottom: 25px;"><strong>All Winning Numbers</strong></h2>
            </div>
        </div>
        </thead>
    </table>

    <br>
    <table id="datatable" class="table table-striped table-bordered" style="width:100%">
        <thead>
            <tr>
                <th>Date</th>
                <th>Time</th>
                <th>1st Place</th>
                <th>2nd Place</th>
                <th>3rd Place</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($results as $item): ?>
                @if($item->tanggal->format('Y-m-d') != $skr && $item->tanggal->format('Y-m-d') != $tom)
                <tr>
                    <td>{{ $item->tanggal->format('d-m-Y') }}</td>
                    <td>{{ $item->time }}</td>
                    <td>{{ $item->angka_1 }}</td>
                    <td>{{ $item->angka_2 }}</td>
                    <td>{{ $item->angka_3 }}</td>




                </tr>
                @endif
            <?php endforeach ?>
        </tbody>


    </table>
</div>


</div>

<br>
<br>
<br>
<br>
<br>

<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <img src="http://versailleslottery.com/images/bnv.png" alt="" class="float-left" height="45">
            <img src="http://versailleslottery.com/images/vip.png" alt="" class="float-right">
        </div>
    </div>
</div>

            
<br>
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script>
    $(document).ready(function() {
    $('#datatable').DataTable({
        ordering:false,
        responsive: true

    })
    ;
} );
</script>
                

                

@endsection